﻿/*
 * Created by SharpDevelop.
 * User: alyss
 * Date: 11/18/2020
 * Time: 5:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace mastermindNew
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.g1c1Label = new System.Windows.Forms.Label();
			this.g1c4Label = new System.Windows.Forms.Label();
			this.g1c3Label = new System.Windows.Forms.Label();
			this.g1c2Label = new System.Windows.Forms.Label();
			this.g2c4Label = new System.Windows.Forms.Label();
			this.g2c3Label = new System.Windows.Forms.Label();
			this.g2c2Label = new System.Windows.Forms.Label();
			this.g2c1Label = new System.Windows.Forms.Label();
			this.g3c4Label = new System.Windows.Forms.Label();
			this.g3c3Label = new System.Windows.Forms.Label();
			this.g3c2Label = new System.Windows.Forms.Label();
			this.g3c1Label = new System.Windows.Forms.Label();
			this.g4c1Label = new System.Windows.Forms.Label();
			this.g6c1Label = new System.Windows.Forms.Label();
			this.g7c1Label = new System.Windows.Forms.Label();
			this.g5c1Label = new System.Windows.Forms.Label();
			this.g8c1Label = new System.Windows.Forms.Label();
			this.g9c1Label = new System.Windows.Forms.Label();
			this.g10c1Label = new System.Windows.Forms.Label();
			this.g8c2Label = new System.Windows.Forms.Label();
			this.g7c4Label = new System.Windows.Forms.Label();
			this.g7c3Label = new System.Windows.Forms.Label();
			this.g7c2Label = new System.Windows.Forms.Label();
			this.g6c4Label = new System.Windows.Forms.Label();
			this.g6c3Label = new System.Windows.Forms.Label();
			this.g6c2Label = new System.Windows.Forms.Label();
			this.g5c4Label = new System.Windows.Forms.Label();
			this.g5c3Label = new System.Windows.Forms.Label();
			this.g5c2Label = new System.Windows.Forms.Label();
			this.g4c4Label = new System.Windows.Forms.Label();
			this.g4c3Label = new System.Windows.Forms.Label();
			this.g4c2Label = new System.Windows.Forms.Label();
			this.g10c4Label = new System.Windows.Forms.Label();
			this.g10c3Label = new System.Windows.Forms.Label();
			this.g10c2Label = new System.Windows.Forms.Label();
			this.g9c4Label = new System.Windows.Forms.Label();
			this.g9c3Label = new System.Windows.Forms.Label();
			this.g9c2Label = new System.Windows.Forms.Label();
			this.g8c4Label = new System.Windows.Forms.Label();
			this.g8c3Label = new System.Windows.Forms.Label();
			this.label41 = new System.Windows.Forms.Label();
			this.label42 = new System.Windows.Forms.Label();
			this.label43 = new System.Windows.Forms.Label();
			this.label44 = new System.Windows.Forms.Label();
			this.label45 = new System.Windows.Forms.Label();
			this.label46 = new System.Windows.Forms.Label();
			this.label47 = new System.Windows.Forms.Label();
			this.label48 = new System.Windows.Forms.Label();
			this.label49 = new System.Windows.Forms.Label();
			this.label50 = new System.Windows.Forms.Label();
			this.solution1Label = new System.Windows.Forms.Label();
			this.solution2Label = new System.Windows.Forms.Label();
			this.solution3Label = new System.Windows.Forms.Label();
			this.solution4Label = new System.Windows.Forms.Label();
			this.label55 = new System.Windows.Forms.Label();
			this.color1GroupBox = new System.Windows.Forms.GroupBox();
			this.g1ComboBox = new System.Windows.Forms.ComboBox();
			this.color2GroupBox = new System.Windows.Forms.GroupBox();
			this.g2ComboBox = new System.Windows.Forms.ComboBox();
			this.color3GroupBox = new System.Windows.Forms.GroupBox();
			this.g3ComboBox = new System.Windows.Forms.ComboBox();
			this.color4GroupBox = new System.Windows.Forms.GroupBox();
			this.g4ComboBox = new System.Windows.Forms.ComboBox();
			this.guessButton = new System.Windows.Forms.Button();
			this.resetButton = new System.Windows.Forms.Button();
			this.g1r1 = new System.Windows.Forms.Label();
			this.g1r2 = new System.Windows.Forms.Label();
			this.g1r3 = new System.Windows.Forms.Label();
			this.g1r4 = new System.Windows.Forms.Label();
			this.g2r1 = new System.Windows.Forms.Label();
			this.g2r2 = new System.Windows.Forms.Label();
			this.g2r3 = new System.Windows.Forms.Label();
			this.g2r4 = new System.Windows.Forms.Label();
			this.g3r1 = new System.Windows.Forms.Label();
			this.g3r2 = new System.Windows.Forms.Label();
			this.g3r3 = new System.Windows.Forms.Label();
			this.g3r4 = new System.Windows.Forms.Label();
			this.g4r1 = new System.Windows.Forms.Label();
			this.g4r2 = new System.Windows.Forms.Label();
			this.g4r3 = new System.Windows.Forms.Label();
			this.g4r4 = new System.Windows.Forms.Label();
			this.g5r1 = new System.Windows.Forms.Label();
			this.g5r2 = new System.Windows.Forms.Label();
			this.g5r3 = new System.Windows.Forms.Label();
			this.g5r4 = new System.Windows.Forms.Label();
			this.g6r1 = new System.Windows.Forms.Label();
			this.g6r2 = new System.Windows.Forms.Label();
			this.g6r3 = new System.Windows.Forms.Label();
			this.g6r4 = new System.Windows.Forms.Label();
			this.g7r1 = new System.Windows.Forms.Label();
			this.g7r2 = new System.Windows.Forms.Label();
			this.g7r4 = new System.Windows.Forms.Label();
			this.g8r1 = new System.Windows.Forms.Label();
			this.g8r2 = new System.Windows.Forms.Label();
			this.g8r3 = new System.Windows.Forms.Label();
			this.g8r4 = new System.Windows.Forms.Label();
			this.g9r1 = new System.Windows.Forms.Label();
			this.g9r2 = new System.Windows.Forms.Label();
			this.g9r3 = new System.Windows.Forms.Label();
			this.g7r3 = new System.Windows.Forms.Label();
			this.g9r4 = new System.Windows.Forms.Label();
			this.g10r3 = new System.Windows.Forms.Label();
			this.g10r2 = new System.Windows.Forms.Label();
			this.g10r1 = new System.Windows.Forms.Label();
			this.g10r4 = new System.Windows.Forms.Label();
			this.color1GroupBox.SuspendLayout();
			this.color2GroupBox.SuspendLayout();
			this.color3GroupBox.SuspendLayout();
			this.color4GroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// g1c1Label
			// 
			this.g1c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g1c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1c1Label.Location = new System.Drawing.Point(16, 9);
			this.g1c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g1c1Label.Name = "g1c1Label";
			this.g1c1Label.Size = new System.Drawing.Size(28, 20);
			this.g1c1Label.TabIndex = 0;
			// 
			// g1c4Label
			// 
			this.g1c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g1c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1c4Label.Location = new System.Drawing.Point(124, 9);
			this.g1c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g1c4Label.Name = "g1c4Label";
			this.g1c4Label.Size = new System.Drawing.Size(28, 20);
			this.g1c4Label.TabIndex = 1;
			// 
			// g1c3Label
			// 
			this.g1c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g1c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1c3Label.Location = new System.Drawing.Point(88, 9);
			this.g1c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g1c3Label.Name = "g1c3Label";
			this.g1c3Label.Size = new System.Drawing.Size(28, 20);
			this.g1c3Label.TabIndex = 2;
			// 
			// g1c2Label
			// 
			this.g1c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g1c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1c2Label.Location = new System.Drawing.Point(52, 9);
			this.g1c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g1c2Label.Name = "g1c2Label";
			this.g1c2Label.Size = new System.Drawing.Size(28, 20);
			this.g1c2Label.TabIndex = 3;
			// 
			// g2c4Label
			// 
			this.g2c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g2c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2c4Label.Location = new System.Drawing.Point(124, 38);
			this.g2c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g2c4Label.Name = "g2c4Label";
			this.g2c4Label.Size = new System.Drawing.Size(28, 20);
			this.g2c4Label.TabIndex = 4;
			// 
			// g2c3Label
			// 
			this.g2c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g2c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2c3Label.Location = new System.Drawing.Point(88, 38);
			this.g2c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g2c3Label.Name = "g2c3Label";
			this.g2c3Label.Size = new System.Drawing.Size(28, 20);
			this.g2c3Label.TabIndex = 5;
			// 
			// g2c2Label
			// 
			this.g2c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g2c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2c2Label.Location = new System.Drawing.Point(52, 38);
			this.g2c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g2c2Label.Name = "g2c2Label";
			this.g2c2Label.Size = new System.Drawing.Size(28, 20);
			this.g2c2Label.TabIndex = 6;
			// 
			// g2c1Label
			// 
			this.g2c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g2c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2c1Label.Location = new System.Drawing.Point(16, 38);
			this.g2c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g2c1Label.Name = "g2c1Label";
			this.g2c1Label.Size = new System.Drawing.Size(28, 20);
			this.g2c1Label.TabIndex = 7;
			// 
			// g3c4Label
			// 
			this.g3c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g3c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3c4Label.Location = new System.Drawing.Point(124, 68);
			this.g3c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g3c4Label.Name = "g3c4Label";
			this.g3c4Label.Size = new System.Drawing.Size(28, 20);
			this.g3c4Label.TabIndex = 8;
			// 
			// g3c3Label
			// 
			this.g3c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g3c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3c3Label.Location = new System.Drawing.Point(88, 68);
			this.g3c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g3c3Label.Name = "g3c3Label";
			this.g3c3Label.Size = new System.Drawing.Size(28, 20);
			this.g3c3Label.TabIndex = 9;
			// 
			// g3c2Label
			// 
			this.g3c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g3c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3c2Label.Location = new System.Drawing.Point(52, 68);
			this.g3c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g3c2Label.Name = "g3c2Label";
			this.g3c2Label.Size = new System.Drawing.Size(28, 20);
			this.g3c2Label.TabIndex = 10;
			// 
			// g3c1Label
			// 
			this.g3c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g3c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3c1Label.Location = new System.Drawing.Point(16, 68);
			this.g3c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g3c1Label.Name = "g3c1Label";
			this.g3c1Label.Size = new System.Drawing.Size(28, 20);
			this.g3c1Label.TabIndex = 11;
			// 
			// g4c1Label
			// 
			this.g4c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g4c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4c1Label.Location = new System.Drawing.Point(16, 97);
			this.g4c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g4c1Label.Name = "g4c1Label";
			this.g4c1Label.Size = new System.Drawing.Size(28, 20);
			this.g4c1Label.TabIndex = 12;
			// 
			// g6c1Label
			// 
			this.g6c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g6c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6c1Label.Location = new System.Drawing.Point(16, 156);
			this.g6c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g6c1Label.Name = "g6c1Label";
			this.g6c1Label.Size = new System.Drawing.Size(28, 20);
			this.g6c1Label.TabIndex = 13;
			// 
			// g7c1Label
			// 
			this.g7c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g7c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7c1Label.Location = new System.Drawing.Point(16, 187);
			this.g7c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g7c1Label.Name = "g7c1Label";
			this.g7c1Label.Size = new System.Drawing.Size(28, 20);
			this.g7c1Label.TabIndex = 14;
			// 
			// g5c1Label
			// 
			this.g5c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g5c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5c1Label.Location = new System.Drawing.Point(16, 127);
			this.g5c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g5c1Label.Name = "g5c1Label";
			this.g5c1Label.Size = new System.Drawing.Size(28, 20);
			this.g5c1Label.TabIndex = 15;
			// 
			// g8c1Label
			// 
			this.g8c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g8c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8c1Label.Location = new System.Drawing.Point(16, 218);
			this.g8c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g8c1Label.Name = "g8c1Label";
			this.g8c1Label.Size = new System.Drawing.Size(28, 20);
			this.g8c1Label.TabIndex = 16;
			// 
			// g9c1Label
			// 
			this.g9c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g9c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9c1Label.Location = new System.Drawing.Point(16, 249);
			this.g9c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g9c1Label.Name = "g9c1Label";
			this.g9c1Label.Size = new System.Drawing.Size(28, 20);
			this.g9c1Label.TabIndex = 17;
			// 
			// g10c1Label
			// 
			this.g10c1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g10c1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10c1Label.Location = new System.Drawing.Point(16, 281);
			this.g10c1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g10c1Label.Name = "g10c1Label";
			this.g10c1Label.Size = new System.Drawing.Size(28, 20);
			this.g10c1Label.TabIndex = 18;
			// 
			// g8c2Label
			// 
			this.g8c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g8c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8c2Label.Location = new System.Drawing.Point(52, 218);
			this.g8c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g8c2Label.Name = "g8c2Label";
			this.g8c2Label.Size = new System.Drawing.Size(28, 20);
			this.g8c2Label.TabIndex = 19;
			// 
			// g7c4Label
			// 
			this.g7c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g7c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7c4Label.Location = new System.Drawing.Point(124, 187);
			this.g7c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g7c4Label.Name = "g7c4Label";
			this.g7c4Label.Size = new System.Drawing.Size(28, 20);
			this.g7c4Label.TabIndex = 20;
			// 
			// g7c3Label
			// 
			this.g7c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g7c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7c3Label.Location = new System.Drawing.Point(88, 187);
			this.g7c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g7c3Label.Name = "g7c3Label";
			this.g7c3Label.Size = new System.Drawing.Size(28, 20);
			this.g7c3Label.TabIndex = 21;
			// 
			// g7c2Label
			// 
			this.g7c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g7c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7c2Label.Location = new System.Drawing.Point(52, 187);
			this.g7c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g7c2Label.Name = "g7c2Label";
			this.g7c2Label.Size = new System.Drawing.Size(28, 20);
			this.g7c2Label.TabIndex = 22;
			// 
			// g6c4Label
			// 
			this.g6c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g6c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6c4Label.Location = new System.Drawing.Point(124, 156);
			this.g6c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g6c4Label.Name = "g6c4Label";
			this.g6c4Label.Size = new System.Drawing.Size(28, 20);
			this.g6c4Label.TabIndex = 23;
			// 
			// g6c3Label
			// 
			this.g6c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g6c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6c3Label.Location = new System.Drawing.Point(88, 156);
			this.g6c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g6c3Label.Name = "g6c3Label";
			this.g6c3Label.Size = new System.Drawing.Size(28, 20);
			this.g6c3Label.TabIndex = 24;
			// 
			// g6c2Label
			// 
			this.g6c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g6c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6c2Label.Location = new System.Drawing.Point(52, 155);
			this.g6c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g6c2Label.Name = "g6c2Label";
			this.g6c2Label.Size = new System.Drawing.Size(28, 20);
			this.g6c2Label.TabIndex = 25;
			// 
			// g5c4Label
			// 
			this.g5c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g5c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5c4Label.Location = new System.Drawing.Point(124, 127);
			this.g5c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g5c4Label.Name = "g5c4Label";
			this.g5c4Label.Size = new System.Drawing.Size(28, 20);
			this.g5c4Label.TabIndex = 26;
			// 
			// g5c3Label
			// 
			this.g5c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g5c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5c3Label.Location = new System.Drawing.Point(88, 127);
			this.g5c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g5c3Label.Name = "g5c3Label";
			this.g5c3Label.Size = new System.Drawing.Size(28, 20);
			this.g5c3Label.TabIndex = 27;
			// 
			// g5c2Label
			// 
			this.g5c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g5c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5c2Label.Location = new System.Drawing.Point(52, 127);
			this.g5c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g5c2Label.Name = "g5c2Label";
			this.g5c2Label.Size = new System.Drawing.Size(28, 20);
			this.g5c2Label.TabIndex = 28;
			// 
			// g4c4Label
			// 
			this.g4c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g4c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4c4Label.Location = new System.Drawing.Point(124, 97);
			this.g4c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g4c4Label.Name = "g4c4Label";
			this.g4c4Label.Size = new System.Drawing.Size(28, 20);
			this.g4c4Label.TabIndex = 29;
			// 
			// g4c3Label
			// 
			this.g4c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g4c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4c3Label.Location = new System.Drawing.Point(88, 97);
			this.g4c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g4c3Label.Name = "g4c3Label";
			this.g4c3Label.Size = new System.Drawing.Size(28, 20);
			this.g4c3Label.TabIndex = 30;
			// 
			// g4c2Label
			// 
			this.g4c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g4c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4c2Label.Location = new System.Drawing.Point(52, 97);
			this.g4c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g4c2Label.Name = "g4c2Label";
			this.g4c2Label.Size = new System.Drawing.Size(28, 20);
			this.g4c2Label.TabIndex = 31;
			// 
			// g10c4Label
			// 
			this.g10c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g10c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10c4Label.Location = new System.Drawing.Point(124, 281);
			this.g10c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g10c4Label.Name = "g10c4Label";
			this.g10c4Label.Size = new System.Drawing.Size(28, 20);
			this.g10c4Label.TabIndex = 32;
			// 
			// g10c3Label
			// 
			this.g10c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g10c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10c3Label.Location = new System.Drawing.Point(88, 281);
			this.g10c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g10c3Label.Name = "g10c3Label";
			this.g10c3Label.Size = new System.Drawing.Size(28, 20);
			this.g10c3Label.TabIndex = 33;
			// 
			// g10c2Label
			// 
			this.g10c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g10c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10c2Label.Location = new System.Drawing.Point(52, 281);
			this.g10c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g10c2Label.Name = "g10c2Label";
			this.g10c2Label.Size = new System.Drawing.Size(28, 20);
			this.g10c2Label.TabIndex = 34;
			// 
			// g9c4Label
			// 
			this.g9c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g9c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9c4Label.Location = new System.Drawing.Point(124, 249);
			this.g9c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g9c4Label.Name = "g9c4Label";
			this.g9c4Label.Size = new System.Drawing.Size(28, 20);
			this.g9c4Label.TabIndex = 35;
			// 
			// g9c3Label
			// 
			this.g9c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g9c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9c3Label.Location = new System.Drawing.Point(88, 249);
			this.g9c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g9c3Label.Name = "g9c3Label";
			this.g9c3Label.Size = new System.Drawing.Size(28, 20);
			this.g9c3Label.TabIndex = 36;
			// 
			// g9c2Label
			// 
			this.g9c2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g9c2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9c2Label.Location = new System.Drawing.Point(52, 249);
			this.g9c2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g9c2Label.Name = "g9c2Label";
			this.g9c2Label.Size = new System.Drawing.Size(28, 20);
			this.g9c2Label.TabIndex = 37;
			// 
			// g8c4Label
			// 
			this.g8c4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g8c4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8c4Label.Location = new System.Drawing.Point(124, 218);
			this.g8c4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g8c4Label.Name = "g8c4Label";
			this.g8c4Label.Size = new System.Drawing.Size(28, 20);
			this.g8c4Label.TabIndex = 38;
			// 
			// g8c3Label
			// 
			this.g8c3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.g8c3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8c3Label.Location = new System.Drawing.Point(88, 218);
			this.g8c3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.g8c3Label.Name = "g8c3Label";
			this.g8c3Label.Size = new System.Drawing.Size(28, 20);
			this.g8c3Label.TabIndex = 39;
			// 
			// label41
			// 
			this.label41.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label41.Location = new System.Drawing.Point(160, 10);
			this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(83, 19);
			this.label41.TabIndex = 40;
			this.label41.Text = "Guess 1";
			this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label42
			// 
			this.label42.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label42.Location = new System.Drawing.Point(160, 186);
			this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label42.Name = "label42";
			this.label42.Size = new System.Drawing.Size(83, 19);
			this.label42.TabIndex = 41;
			this.label42.Text = "Guess 7";
			this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label43
			// 
			this.label43.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label43.Location = new System.Drawing.Point(160, 156);
			this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(83, 19);
			this.label43.TabIndex = 42;
			this.label43.Text = "Guess 6";
			this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label44
			// 
			this.label44.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label44.Location = new System.Drawing.Point(160, 126);
			this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(83, 19);
			this.label44.TabIndex = 43;
			this.label44.Text = "Guess 5";
			this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label45
			// 
			this.label45.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label45.Location = new System.Drawing.Point(160, 96);
			this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(83, 19);
			this.label45.TabIndex = 44;
			this.label45.Text = "Guess 4";
			this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label46
			// 
			this.label46.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label46.Location = new System.Drawing.Point(160, 67);
			this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(83, 19);
			this.label46.TabIndex = 45;
			this.label46.Text = "Guess 3";
			this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label47
			// 
			this.label47.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label47.Location = new System.Drawing.Point(160, 38);
			this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(83, 19);
			this.label47.TabIndex = 46;
			this.label47.Text = "Guess 2";
			this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label48
			// 
			this.label48.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label48.Location = new System.Drawing.Point(160, 280);
			this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label48.Name = "label48";
			this.label48.Size = new System.Drawing.Size(83, 19);
			this.label48.TabIndex = 47;
			this.label48.Text = "Guess 10";
			this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label49
			// 
			this.label49.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label49.Location = new System.Drawing.Point(160, 250);
			this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label49.Name = "label49";
			this.label49.Size = new System.Drawing.Size(83, 19);
			this.label49.TabIndex = 48;
			this.label49.Text = "Guess 9";
			this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label50
			// 
			this.label50.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label50.Location = new System.Drawing.Point(160, 218);
			this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label50.Name = "label50";
			this.label50.Size = new System.Drawing.Size(83, 19);
			this.label50.TabIndex = 49;
			this.label50.Text = "Guess 8";
			this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// solution1Label
			// 
			this.solution1Label.BackColor = System.Drawing.Color.FloralWhite;
			this.solution1Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.solution1Label.Location = new System.Drawing.Point(481, 302);
			this.solution1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.solution1Label.Name = "solution1Label";
			this.solution1Label.Size = new System.Drawing.Size(28, 20);
			this.solution1Label.TabIndex = 50;
			// 
			// solution2Label
			// 
			this.solution2Label.BackColor = System.Drawing.Color.FloralWhite;
			this.solution2Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.solution2Label.Location = new System.Drawing.Point(517, 302);
			this.solution2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.solution2Label.Name = "solution2Label";
			this.solution2Label.Size = new System.Drawing.Size(28, 20);
			this.solution2Label.TabIndex = 51;
			// 
			// solution3Label
			// 
			this.solution3Label.BackColor = System.Drawing.Color.FloralWhite;
			this.solution3Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.solution3Label.Location = new System.Drawing.Point(553, 302);
			this.solution3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.solution3Label.Name = "solution3Label";
			this.solution3Label.Size = new System.Drawing.Size(28, 20);
			this.solution3Label.TabIndex = 52;
			// 
			// solution4Label
			// 
			this.solution4Label.BackColor = System.Drawing.Color.FloralWhite;
			this.solution4Label.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.solution4Label.Location = new System.Drawing.Point(589, 302);
			this.solution4Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.solution4Label.Name = "solution4Label";
			this.solution4Label.Size = new System.Drawing.Size(28, 20);
			this.solution4Label.TabIndex = 53;
			// 
			// label55
			// 
			this.label55.Font = new System.Drawing.Font("Mongolian Baiti", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label55.Location = new System.Drawing.Point(481, 281);
			this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label55.Name = "label55";
			this.label55.Size = new System.Drawing.Size(136, 19);
			this.label55.TabIndex = 54;
			this.label55.Text = "Solution";
			this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// color1GroupBox
			// 
			this.color1GroupBox.Controls.Add(this.g1ComboBox);
			this.color1GroupBox.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color1GroupBox.Location = new System.Drawing.Point(365, 6);
			this.color1GroupBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color1GroupBox.Name = "color1GroupBox";
			this.color1GroupBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color1GroupBox.Size = new System.Drawing.Size(252, 62);
			this.color1GroupBox.TabIndex = 55;
			this.color1GroupBox.TabStop = false;
			this.color1GroupBox.Text = "Color 1";
			// 
			// g1ComboBox
			// 
			this.g1ComboBox.FormattingEnabled = true;
			this.g1ComboBox.Items.AddRange(new object[] {
									"Gold",
									"Blue",
									"Pink",
									"Green",
									"Red",
									"Black"});
			this.g1ComboBox.Location = new System.Drawing.Point(36, 20);
			this.g1ComboBox.Name = "g1ComboBox";
			this.g1ComboBox.Size = new System.Drawing.Size(180, 21);
			this.g1ComboBox.TabIndex = 0;
			// 
			// color2GroupBox
			// 
			this.color2GroupBox.Controls.Add(this.g2ComboBox);
			this.color2GroupBox.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color2GroupBox.Location = new System.Drawing.Point(365, 74);
			this.color2GroupBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color2GroupBox.Name = "color2GroupBox";
			this.color2GroupBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color2GroupBox.Size = new System.Drawing.Size(252, 62);
			this.color2GroupBox.TabIndex = 56;
			this.color2GroupBox.TabStop = false;
			this.color2GroupBox.Text = "Color 2";
			// 
			// g2ComboBox
			// 
			this.g2ComboBox.FormattingEnabled = true;
			this.g2ComboBox.Items.AddRange(new object[] {
									"Gold",
									"Blue",
									"Pink",
									"Green",
									"Red",
									"Black"});
			this.g2ComboBox.Location = new System.Drawing.Point(36, 20);
			this.g2ComboBox.Name = "g2ComboBox";
			this.g2ComboBox.Size = new System.Drawing.Size(180, 21);
			this.g2ComboBox.TabIndex = 2;
			// 
			// color3GroupBox
			// 
			this.color3GroupBox.Controls.Add(this.g3ComboBox);
			this.color3GroupBox.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color3GroupBox.Location = new System.Drawing.Point(365, 143);
			this.color3GroupBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color3GroupBox.Name = "color3GroupBox";
			this.color3GroupBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color3GroupBox.Size = new System.Drawing.Size(252, 62);
			this.color3GroupBox.TabIndex = 56;
			this.color3GroupBox.TabStop = false;
			this.color3GroupBox.Text = "Color 3";
			// 
			// g3ComboBox
			// 
			this.g3ComboBox.FormattingEnabled = true;
			this.g3ComboBox.Items.AddRange(new object[] {
									"Gold",
									"Blue",
									"Pink",
									"Green",
									"Red",
									"Black"});
			this.g3ComboBox.Location = new System.Drawing.Point(36, 20);
			this.g3ComboBox.Name = "g3ComboBox";
			this.g3ComboBox.Size = new System.Drawing.Size(180, 21);
			this.g3ComboBox.TabIndex = 2;
			// 
			// color4GroupBox
			// 
			this.color4GroupBox.Controls.Add(this.g4ComboBox);
			this.color4GroupBox.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color4GroupBox.Location = new System.Drawing.Point(365, 211);
			this.color4GroupBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color4GroupBox.Name = "color4GroupBox";
			this.color4GroupBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.color4GroupBox.Size = new System.Drawing.Size(252, 62);
			this.color4GroupBox.TabIndex = 56;
			this.color4GroupBox.TabStop = false;
			this.color4GroupBox.Text = "Color 4";
			// 
			// g4ComboBox
			// 
			this.g4ComboBox.FormattingEnabled = true;
			this.g4ComboBox.Items.AddRange(new object[] {
									"Gold",
									"Blue",
									"Pink",
									"Green",
									"Red",
									"Black"});
			this.g4ComboBox.Location = new System.Drawing.Point(36, 20);
			this.g4ComboBox.Name = "g4ComboBox";
			this.g4ComboBox.Size = new System.Drawing.Size(180, 21);
			this.g4ComboBox.TabIndex = 1;
			// 
			// guessButton
			// 
			this.guessButton.BackColor = System.Drawing.Color.FloralWhite;
			this.guessButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.guessButton.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.guessButton.Location = new System.Drawing.Point(321, 280);
			this.guessButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.guessButton.Name = "guessButton";
			this.guessButton.Size = new System.Drawing.Size(72, 41);
			this.guessButton.TabIndex = 57;
			this.guessButton.Text = "Guess";
			this.guessButton.UseVisualStyleBackColor = false;
			this.guessButton.Click += new System.EventHandler(this.GuessButtonClick);
			// 
			// resetButton
			// 
			this.resetButton.BackColor = System.Drawing.Color.FloralWhite;
			this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.resetButton.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.resetButton.Location = new System.Drawing.Point(401, 280);
			this.resetButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.resetButton.Name = "resetButton";
			this.resetButton.Size = new System.Drawing.Size(72, 41);
			this.resetButton.TabIndex = 58;
			this.resetButton.Text = "Reset";
			this.resetButton.UseVisualStyleBackColor = false;
			this.resetButton.Click += new System.EventHandler(this.ResetButtonClick);
			// 
			// g1r1
			// 
			this.g1r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g1r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1r1.Location = new System.Drawing.Point(250, 12);
			this.g1r1.Name = "g1r1";
			this.g1r1.Size = new System.Drawing.Size(10, 10);
			this.g1r1.TabIndex = 59;
			// 
			// g1r2
			// 
			this.g1r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g1r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1r2.Location = new System.Drawing.Point(266, 12);
			this.g1r2.Name = "g1r2";
			this.g1r2.Size = new System.Drawing.Size(10, 10);
			this.g1r2.TabIndex = 60;
			// 
			// g1r3
			// 
			this.g1r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g1r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1r3.Location = new System.Drawing.Point(282, 12);
			this.g1r3.Name = "g1r3";
			this.g1r3.Size = new System.Drawing.Size(10, 10);
			this.g1r3.TabIndex = 61;
			// 
			// g1r4
			// 
			this.g1r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g1r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g1r4.Location = new System.Drawing.Point(298, 12);
			this.g1r4.Name = "g1r4";
			this.g1r4.Size = new System.Drawing.Size(10, 10);
			this.g1r4.TabIndex = 62;
			// 
			// g2r1
			// 
			this.g2r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g2r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2r1.Location = new System.Drawing.Point(250, 40);
			this.g2r1.Name = "g2r1";
			this.g2r1.Size = new System.Drawing.Size(10, 10);
			this.g2r1.TabIndex = 63;
			// 
			// g2r2
			// 
			this.g2r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g2r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2r2.Location = new System.Drawing.Point(266, 40);
			this.g2r2.Name = "g2r2";
			this.g2r2.Size = new System.Drawing.Size(10, 10);
			this.g2r2.TabIndex = 64;
			// 
			// g2r3
			// 
			this.g2r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g2r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2r3.Location = new System.Drawing.Point(282, 40);
			this.g2r3.Name = "g2r3";
			this.g2r3.Size = new System.Drawing.Size(10, 10);
			this.g2r3.TabIndex = 65;
			// 
			// g2r4
			// 
			this.g2r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g2r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g2r4.Location = new System.Drawing.Point(298, 40);
			this.g2r4.Name = "g2r4";
			this.g2r4.Size = new System.Drawing.Size(10, 10);
			this.g2r4.TabIndex = 66;
			// 
			// g3r1
			// 
			this.g3r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g3r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3r1.Location = new System.Drawing.Point(250, 69);
			this.g3r1.Name = "g3r1";
			this.g3r1.Size = new System.Drawing.Size(10, 10);
			this.g3r1.TabIndex = 67;
			// 
			// g3r2
			// 
			this.g3r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g3r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3r2.Location = new System.Drawing.Point(266, 69);
			this.g3r2.Name = "g3r2";
			this.g3r2.Size = new System.Drawing.Size(10, 10);
			this.g3r2.TabIndex = 68;
			// 
			// g3r3
			// 
			this.g3r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g3r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3r3.Location = new System.Drawing.Point(282, 69);
			this.g3r3.Name = "g3r3";
			this.g3r3.Size = new System.Drawing.Size(10, 10);
			this.g3r3.TabIndex = 69;
			// 
			// g3r4
			// 
			this.g3r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g3r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g3r4.Location = new System.Drawing.Point(298, 69);
			this.g3r4.Name = "g3r4";
			this.g3r4.Size = new System.Drawing.Size(10, 10);
			this.g3r4.TabIndex = 70;
			// 
			// g4r1
			// 
			this.g4r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g4r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4r1.Location = new System.Drawing.Point(250, 98);
			this.g4r1.Name = "g4r1";
			this.g4r1.Size = new System.Drawing.Size(10, 10);
			this.g4r1.TabIndex = 71;
			// 
			// g4r2
			// 
			this.g4r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g4r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4r2.Location = new System.Drawing.Point(266, 98);
			this.g4r2.Name = "g4r2";
			this.g4r2.Size = new System.Drawing.Size(10, 10);
			this.g4r2.TabIndex = 72;
			// 
			// g4r3
			// 
			this.g4r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g4r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4r3.Location = new System.Drawing.Point(282, 98);
			this.g4r3.Name = "g4r3";
			this.g4r3.Size = new System.Drawing.Size(10, 10);
			this.g4r3.TabIndex = 73;
			// 
			// g4r4
			// 
			this.g4r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g4r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g4r4.Location = new System.Drawing.Point(298, 98);
			this.g4r4.Name = "g4r4";
			this.g4r4.Size = new System.Drawing.Size(10, 10);
			this.g4r4.TabIndex = 74;
			// 
			// g5r1
			// 
			this.g5r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g5r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5r1.Location = new System.Drawing.Point(250, 128);
			this.g5r1.Name = "g5r1";
			this.g5r1.Size = new System.Drawing.Size(10, 10);
			this.g5r1.TabIndex = 75;
			// 
			// g5r2
			// 
			this.g5r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g5r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5r2.Location = new System.Drawing.Point(266, 128);
			this.g5r2.Name = "g5r2";
			this.g5r2.Size = new System.Drawing.Size(10, 10);
			this.g5r2.TabIndex = 76;
			// 
			// g5r3
			// 
			this.g5r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g5r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5r3.Location = new System.Drawing.Point(282, 128);
			this.g5r3.Name = "g5r3";
			this.g5r3.Size = new System.Drawing.Size(10, 10);
			this.g5r3.TabIndex = 77;
			// 
			// g5r4
			// 
			this.g5r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g5r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g5r4.Location = new System.Drawing.Point(298, 128);
			this.g5r4.Name = "g5r4";
			this.g5r4.Size = new System.Drawing.Size(10, 10);
			this.g5r4.TabIndex = 78;
			// 
			// g6r1
			// 
			this.g6r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g6r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6r1.Location = new System.Drawing.Point(250, 158);
			this.g6r1.Name = "g6r1";
			this.g6r1.Size = new System.Drawing.Size(10, 10);
			this.g6r1.TabIndex = 79;
			// 
			// g6r2
			// 
			this.g6r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g6r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6r2.Location = new System.Drawing.Point(266, 158);
			this.g6r2.Name = "g6r2";
			this.g6r2.Size = new System.Drawing.Size(10, 10);
			this.g6r2.TabIndex = 80;
			// 
			// g6r3
			// 
			this.g6r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g6r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6r3.Location = new System.Drawing.Point(282, 158);
			this.g6r3.Name = "g6r3";
			this.g6r3.Size = new System.Drawing.Size(10, 10);
			this.g6r3.TabIndex = 81;
			// 
			// g6r4
			// 
			this.g6r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g6r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g6r4.Location = new System.Drawing.Point(298, 158);
			this.g6r4.Name = "g6r4";
			this.g6r4.Size = new System.Drawing.Size(10, 10);
			this.g6r4.TabIndex = 82;
			// 
			// g7r1
			// 
			this.g7r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g7r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7r1.Location = new System.Drawing.Point(250, 188);
			this.g7r1.Name = "g7r1";
			this.g7r1.Size = new System.Drawing.Size(10, 10);
			this.g7r1.TabIndex = 83;
			// 
			// g7r2
			// 
			this.g7r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g7r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7r2.Location = new System.Drawing.Point(266, 188);
			this.g7r2.Name = "g7r2";
			this.g7r2.Size = new System.Drawing.Size(10, 10);
			this.g7r2.TabIndex = 84;
			// 
			// g7r4
			// 
			this.g7r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g7r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7r4.Location = new System.Drawing.Point(298, 188);
			this.g7r4.Name = "g7r4";
			this.g7r4.Size = new System.Drawing.Size(10, 10);
			this.g7r4.TabIndex = 85;
			// 
			// g8r1
			// 
			this.g8r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g8r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8r1.Location = new System.Drawing.Point(250, 220);
			this.g8r1.Name = "g8r1";
			this.g8r1.Size = new System.Drawing.Size(10, 10);
			this.g8r1.TabIndex = 86;
			// 
			// g8r2
			// 
			this.g8r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g8r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8r2.Location = new System.Drawing.Point(266, 220);
			this.g8r2.Name = "g8r2";
			this.g8r2.Size = new System.Drawing.Size(10, 10);
			this.g8r2.TabIndex = 87;
			// 
			// g8r3
			// 
			this.g8r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g8r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8r3.Location = new System.Drawing.Point(282, 220);
			this.g8r3.Name = "g8r3";
			this.g8r3.Size = new System.Drawing.Size(10, 10);
			this.g8r3.TabIndex = 88;
			// 
			// g8r4
			// 
			this.g8r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g8r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g8r4.Location = new System.Drawing.Point(298, 220);
			this.g8r4.Name = "g8r4";
			this.g8r4.Size = new System.Drawing.Size(10, 10);
			this.g8r4.TabIndex = 89;
			// 
			// g9r1
			// 
			this.g9r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g9r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9r1.Location = new System.Drawing.Point(250, 252);
			this.g9r1.Name = "g9r1";
			this.g9r1.Size = new System.Drawing.Size(10, 10);
			this.g9r1.TabIndex = 90;
			// 
			// g9r2
			// 
			this.g9r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g9r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9r2.Location = new System.Drawing.Point(266, 252);
			this.g9r2.Name = "g9r2";
			this.g9r2.Size = new System.Drawing.Size(10, 10);
			this.g9r2.TabIndex = 91;
			// 
			// g9r3
			// 
			this.g9r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g9r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9r3.Location = new System.Drawing.Point(282, 252);
			this.g9r3.Name = "g9r3";
			this.g9r3.Size = new System.Drawing.Size(10, 10);
			this.g9r3.TabIndex = 92;
			// 
			// g7r3
			// 
			this.g7r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g7r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g7r3.Location = new System.Drawing.Point(282, 188);
			this.g7r3.Name = "g7r3";
			this.g7r3.Size = new System.Drawing.Size(10, 10);
			this.g7r3.TabIndex = 93;
			// 
			// g9r4
			// 
			this.g9r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g9r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g9r4.Location = new System.Drawing.Point(298, 252);
			this.g9r4.Name = "g9r4";
			this.g9r4.Size = new System.Drawing.Size(10, 10);
			this.g9r4.TabIndex = 94;
			// 
			// g10r3
			// 
			this.g10r3.BackColor = System.Drawing.Color.FloralWhite;
			this.g10r3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10r3.Location = new System.Drawing.Point(282, 282);
			this.g10r3.Name = "g10r3";
			this.g10r3.Size = new System.Drawing.Size(10, 10);
			this.g10r3.TabIndex = 95;
			// 
			// g10r2
			// 
			this.g10r2.BackColor = System.Drawing.Color.FloralWhite;
			this.g10r2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10r2.Location = new System.Drawing.Point(266, 282);
			this.g10r2.Name = "g10r2";
			this.g10r2.Size = new System.Drawing.Size(10, 10);
			this.g10r2.TabIndex = 96;
			// 
			// g10r1
			// 
			this.g10r1.BackColor = System.Drawing.Color.FloralWhite;
			this.g10r1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10r1.Location = new System.Drawing.Point(250, 281);
			this.g10r1.Name = "g10r1";
			this.g10r1.Size = new System.Drawing.Size(10, 10);
			this.g10r1.TabIndex = 97;
			// 
			// g10r4
			// 
			this.g10r4.BackColor = System.Drawing.Color.FloralWhite;
			this.g10r4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.g10r4.Location = new System.Drawing.Point(298, 282);
			this.g10r4.Name = "g10r4";
			this.g10r4.Size = new System.Drawing.Size(10, 10);
			this.g10r4.TabIndex = 98;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightBlue;
			this.ClientSize = new System.Drawing.Size(633, 331);
			this.Controls.Add(this.g10r4);
			this.Controls.Add(this.g10r1);
			this.Controls.Add(this.g10r3);
			this.Controls.Add(this.g10r2);
			this.Controls.Add(this.g9r4);
			this.Controls.Add(this.g7r3);
			this.Controls.Add(this.g9r3);
			this.Controls.Add(this.g9r2);
			this.Controls.Add(this.g9r1);
			this.Controls.Add(this.g8r4);
			this.Controls.Add(this.g8r3);
			this.Controls.Add(this.g8r2);
			this.Controls.Add(this.g8r1);
			this.Controls.Add(this.g7r4);
			this.Controls.Add(this.g7r2);
			this.Controls.Add(this.g7r1);
			this.Controls.Add(this.g6r4);
			this.Controls.Add(this.g6r3);
			this.Controls.Add(this.g6r2);
			this.Controls.Add(this.g6r1);
			this.Controls.Add(this.g5r4);
			this.Controls.Add(this.g5r3);
			this.Controls.Add(this.g5r2);
			this.Controls.Add(this.g5r1);
			this.Controls.Add(this.g4r4);
			this.Controls.Add(this.g4r3);
			this.Controls.Add(this.g4r2);
			this.Controls.Add(this.g4r1);
			this.Controls.Add(this.g3r4);
			this.Controls.Add(this.g3r3);
			this.Controls.Add(this.g3r2);
			this.Controls.Add(this.g3r1);
			this.Controls.Add(this.g2r4);
			this.Controls.Add(this.g2r3);
			this.Controls.Add(this.g2r2);
			this.Controls.Add(this.g2r1);
			this.Controls.Add(this.g1r4);
			this.Controls.Add(this.g1r3);
			this.Controls.Add(this.g1r2);
			this.Controls.Add(this.g1r1);
			this.Controls.Add(this.resetButton);
			this.Controls.Add(this.guessButton);
			this.Controls.Add(this.color2GroupBox);
			this.Controls.Add(this.color3GroupBox);
			this.Controls.Add(this.color4GroupBox);
			this.Controls.Add(this.color1GroupBox);
			this.Controls.Add(this.label55);
			this.Controls.Add(this.solution4Label);
			this.Controls.Add(this.solution3Label);
			this.Controls.Add(this.solution2Label);
			this.Controls.Add(this.solution1Label);
			this.Controls.Add(this.label50);
			this.Controls.Add(this.label49);
			this.Controls.Add(this.label48);
			this.Controls.Add(this.label47);
			this.Controls.Add(this.label46);
			this.Controls.Add(this.label45);
			this.Controls.Add(this.label44);
			this.Controls.Add(this.label43);
			this.Controls.Add(this.label42);
			this.Controls.Add(this.label41);
			this.Controls.Add(this.g8c3Label);
			this.Controls.Add(this.g8c4Label);
			this.Controls.Add(this.g9c2Label);
			this.Controls.Add(this.g9c3Label);
			this.Controls.Add(this.g9c4Label);
			this.Controls.Add(this.g10c2Label);
			this.Controls.Add(this.g10c3Label);
			this.Controls.Add(this.g10c4Label);
			this.Controls.Add(this.g4c2Label);
			this.Controls.Add(this.g4c3Label);
			this.Controls.Add(this.g4c4Label);
			this.Controls.Add(this.g5c2Label);
			this.Controls.Add(this.g5c3Label);
			this.Controls.Add(this.g5c4Label);
			this.Controls.Add(this.g6c2Label);
			this.Controls.Add(this.g6c3Label);
			this.Controls.Add(this.g6c4Label);
			this.Controls.Add(this.g7c2Label);
			this.Controls.Add(this.g7c3Label);
			this.Controls.Add(this.g7c4Label);
			this.Controls.Add(this.g8c2Label);
			this.Controls.Add(this.g10c1Label);
			this.Controls.Add(this.g9c1Label);
			this.Controls.Add(this.g8c1Label);
			this.Controls.Add(this.g5c1Label);
			this.Controls.Add(this.g7c1Label);
			this.Controls.Add(this.g6c1Label);
			this.Controls.Add(this.g4c1Label);
			this.Controls.Add(this.g3c1Label);
			this.Controls.Add(this.g3c2Label);
			this.Controls.Add(this.g3c3Label);
			this.Controls.Add(this.g3c4Label);
			this.Controls.Add(this.g2c1Label);
			this.Controls.Add(this.g2c2Label);
			this.Controls.Add(this.g2c3Label);
			this.Controls.Add(this.g2c4Label);
			this.Controls.Add(this.g1c2Label);
			this.Controls.Add(this.g1c3Label);
			this.Controls.Add(this.g1c4Label);
			this.Controls.Add(this.g1c1Label);
			this.Font = new System.Drawing.Font("Mongolian Baiti", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "MainForm";
			this.Text = "Mastermind";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.color1GroupBox.ResumeLayout(false);
			this.color2GroupBox.ResumeLayout(false);
			this.color3GroupBox.ResumeLayout(false);
			this.color4GroupBox.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label g10r4;
		private System.Windows.Forms.Label g10r1;
		private System.Windows.Forms.Label g10r2;
		private System.Windows.Forms.Label g10r3;
		private System.Windows.Forms.Label g9r4;
		private System.Windows.Forms.Label g7r3;
		private System.Windows.Forms.Label g9r3;
		private System.Windows.Forms.Label g9r2;
		private System.Windows.Forms.Label g9r1;
		private System.Windows.Forms.Label g8r4;
		private System.Windows.Forms.Label g8r3;
		private System.Windows.Forms.Label g8r2;
		private System.Windows.Forms.Label g8r1;
		private System.Windows.Forms.Label g7r4;
		private System.Windows.Forms.Label g7r2;
		private System.Windows.Forms.Label g7r1;
		private System.Windows.Forms.Label g6r4;
		private System.Windows.Forms.Label g6r3;
		private System.Windows.Forms.Label g6r2;
		private System.Windows.Forms.Label g6r1;
		private System.Windows.Forms.Label g5r4;
		private System.Windows.Forms.Label g5r3;
		private System.Windows.Forms.Label g5r2;
		private System.Windows.Forms.Label g5r1;
		private System.Windows.Forms.Label g4r4;
		private System.Windows.Forms.Label g4r3;
		private System.Windows.Forms.Label g4r2;
		private System.Windows.Forms.Label g4r1;
		private System.Windows.Forms.Label g3r4;
		private System.Windows.Forms.Label g3r3;
		private System.Windows.Forms.Label g3r2;
		private System.Windows.Forms.Label g3r1;
		private System.Windows.Forms.Label g2r4;
		private System.Windows.Forms.Label g2r3;
		private System.Windows.Forms.Label g2r2;
		private System.Windows.Forms.Label g2r1;
		private System.Windows.Forms.Label g1r4;
		private System.Windows.Forms.Label g1r3;
		private System.Windows.Forms.Label g1r2;
		private System.Windows.Forms.Label g1r1;
		private System.Windows.Forms.ComboBox g4ComboBox;
		private System.Windows.Forms.ComboBox g3ComboBox;
		private System.Windows.Forms.ComboBox g2ComboBox;
		private System.Windows.Forms.ComboBox g1ComboBox;
		private System.Windows.Forms.Button resetButton;
		private System.Windows.Forms.Button guessButton;
		private System.Windows.Forms.GroupBox color4GroupBox;
		private System.Windows.Forms.GroupBox color3GroupBox;
		private System.Windows.Forms.GroupBox color2GroupBox;
		private System.Windows.Forms.GroupBox color1GroupBox;
		private System.Windows.Forms.Label label55;
		private System.Windows.Forms.Label solution4Label;
		private System.Windows.Forms.Label solution3Label;
		private System.Windows.Forms.Label solution2Label;
		private System.Windows.Forms.Label solution1Label;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label g8c3Label;
		private System.Windows.Forms.Label g8c4Label;
		private System.Windows.Forms.Label g9c2Label;
		private System.Windows.Forms.Label g9c3Label;
		private System.Windows.Forms.Label g9c4Label;
		private System.Windows.Forms.Label g10c2Label;
		private System.Windows.Forms.Label g10c3Label;
		private System.Windows.Forms.Label g10c4Label;
		private System.Windows.Forms.Label g4c2Label;
		private System.Windows.Forms.Label g4c3Label;
		private System.Windows.Forms.Label g4c4Label;
		private System.Windows.Forms.Label g5c2Label;
		private System.Windows.Forms.Label g5c3Label;
		private System.Windows.Forms.Label g5c4Label;
		private System.Windows.Forms.Label g6c2Label;
		private System.Windows.Forms.Label g6c3Label;
		private System.Windows.Forms.Label g6c4Label;
		private System.Windows.Forms.Label g7c2Label;
		private System.Windows.Forms.Label g7c3Label;
		private System.Windows.Forms.Label g7c4Label;
		private System.Windows.Forms.Label g8c2Label;
		private System.Windows.Forms.Label g10c1Label;
		private System.Windows.Forms.Label g9c1Label;
		private System.Windows.Forms.Label g8c1Label;
		private System.Windows.Forms.Label g5c1Label;
		private System.Windows.Forms.Label g7c1Label;
		private System.Windows.Forms.Label g6c1Label;
		private System.Windows.Forms.Label g4c1Label;
		private System.Windows.Forms.Label g3c1Label;
		private System.Windows.Forms.Label g3c2Label;
		private System.Windows.Forms.Label g3c3Label;
		private System.Windows.Forms.Label g3c4Label;
		private System.Windows.Forms.Label g2c1Label;
		private System.Windows.Forms.Label g2c2Label;
		private System.Windows.Forms.Label g2c3Label;
		private System.Windows.Forms.Label g2c4Label;
		private System.Windows.Forms.Label g1c2Label;
		private System.Windows.Forms.Label g1c3Label;
		private System.Windows.Forms.Label g1c4Label;
		private System.Windows.Forms.Label g1c1Label;
	}
}
