﻿/*
 * Created by SharpDevelop.
 * User: alyss
 * Date: 11/18/2020
 * Time: 5:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace mastermindNew
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		//class variables
		int g1, g2, g3, g4; //"g" represents "Guess"
		int s1,s2,s3, s4; //"s" represents "Solution"
		int turn; //"turn" counts the number of turns a user has gone through; max is 10;
		bool solution; //Correct solution?
		bool s1Used, s2Used, s3Used, s4Used; //solution variable used?
		Random AnsGen = new Random(); //Randomized number generator for solution variables
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//Sets variable values upon load
		void MainFormLoad(object sender, EventArgs e)
		{
			//Initialize variables
			turn = 0;
			g1 = 0;
			g2 = 0;
			g3 = 0;
			g4 = 0;
			s1 = -1; //Solution variables pre-defined as -1 in case GenerateSolution() fails.
			s2 = -1;
			s3 = -1;
			s4 = -1;
			solution = false;
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			//Initialize solution
			GenerateSolution();
		}
		
		//Proccess user guesses based on turn
		void GuessButtonClick(object sender, EventArgs e)
		{
			turn++;
			
			if(turn < 11) //This is "11" because the turn mechanism adds one right away.
				//I could make turn "-1" and the "11" to a "10" but I don't want to.
			{
				switch(turn) //This fucker just determines where to put the colors in the guess.
				{
					case 1:
						Guess1();
						break;
					case 2:
						Guess2();
						break;
					case 3:
						Guess3();
						break;
					case 4:
						Guess4();
						break;
					case 5:
						Guess5();
						break;
					case 6:
						Guess6();
						break;
					case 7:
						Guess7();
						break;
					case 8:
						Guess8();
						break;
					case 9:
						Guess9();
						break;
					case 10:
						Guess10();
						break;
				}
			}
			else if(turn >= 11) //This shows the solution if the user uses up their turns.
			{
				ShowSolution();
				MessageBox.Show("Good game, but you are out of turns! Reset to try again!");
			}
			
			CheckSolution(); //Check if guess is correct
		
			if(solution) //This shows the solution if the user gets it right.
			{
				ShowSolution();
				MessageBox.Show("Good game! You got the right answer! Reset to play again!");
			}
			
			DisplayResults(); //Display guess results
		}
		
		//Reset the game for replay
		
		void ResetButtonClick(object sender, EventArgs e)
		{
			//Reset guess blocks
			
			g1c1Label.BackColor = Color.FloralWhite;
			g1c2Label.BackColor = Color.FloralWhite;
			g1c3Label.BackColor = Color.FloralWhite;
			g1c4Label.BackColor = Color.FloralWhite;
			
			g2c1Label.BackColor = Color.FloralWhite;
			g2c2Label.BackColor = Color.FloralWhite;
			g2c3Label.BackColor = Color.FloralWhite;
			g2c4Label.BackColor = Color.FloralWhite;
			
			g3c1Label.BackColor = Color.FloralWhite;
			g3c2Label.BackColor = Color.FloralWhite;
			g3c3Label.BackColor = Color.FloralWhite;
			g3c4Label.BackColor = Color.FloralWhite;
			
			g4c1Label.BackColor = Color.FloralWhite;
			g4c2Label.BackColor = Color.FloralWhite;
			g4c3Label.BackColor = Color.FloralWhite;
			g4c4Label.BackColor = Color.FloralWhite;
			
			g5c1Label.BackColor = Color.FloralWhite;
			g5c2Label.BackColor = Color.FloralWhite;
			g5c3Label.BackColor = Color.FloralWhite;
			g5c4Label.BackColor = Color.FloralWhite;
			
			g6c1Label.BackColor = Color.FloralWhite;
			g6c2Label.BackColor = Color.FloralWhite;
			g6c3Label.BackColor = Color.FloralWhite;
			g6c4Label.BackColor = Color.FloralWhite;
			
			g7c1Label.BackColor = Color.FloralWhite;
			g7c2Label.BackColor = Color.FloralWhite;
			g7c3Label.BackColor = Color.FloralWhite;
			g7c4Label.BackColor = Color.FloralWhite;
			
			g8c1Label.BackColor = Color.FloralWhite;
			g8c2Label.BackColor = Color.FloralWhite;
			g8c3Label.BackColor = Color.FloralWhite;
			g8c4Label.BackColor = Color.FloralWhite;
			
			g9c1Label.BackColor = Color.FloralWhite;
			g9c2Label.BackColor = Color.FloralWhite;
			g9c3Label.BackColor = Color.FloralWhite;
			g9c4Label.BackColor = Color.FloralWhite;
			
			g10c1Label.BackColor = Color.FloralWhite;
			g10c2Label.BackColor = Color.FloralWhite;
			g10c3Label.BackColor = Color.FloralWhite;
			g10c4Label.BackColor = Color.FloralWhite;
			
			//Reset result blocks
			g1r1.BackColor = Color.FloralWhite;
			g1r2.BackColor = Color.FloralWhite;
			g1r3.BackColor = Color.FloralWhite;
			g1r4.BackColor = Color.FloralWhite;
			
			g2r1.BackColor = Color.FloralWhite;
			g2r2.BackColor = Color.FloralWhite;
			g2r3.BackColor = Color.FloralWhite;
			g2r4.BackColor = Color.FloralWhite;
			
			g3r1.BackColor = Color.FloralWhite;
			g3r2.BackColor = Color.FloralWhite;
			g3r3.BackColor = Color.FloralWhite;
			g3r4.BackColor = Color.FloralWhite;
			
			g4r1.BackColor = Color.FloralWhite;
			g4r2.BackColor = Color.FloralWhite;
			g4r3.BackColor = Color.FloralWhite;
			g4r4.BackColor = Color.FloralWhite;
			
			g5r1.BackColor = Color.FloralWhite;
			g5r2.BackColor = Color.FloralWhite;
			g5r3.BackColor = Color.FloralWhite;
			g5r4.BackColor = Color.FloralWhite;
			
			g6r1.BackColor = Color.FloralWhite;
			g6r2.BackColor = Color.FloralWhite;
			g6r3.BackColor = Color.FloralWhite;
			g6r4.BackColor = Color.FloralWhite;
			
			g7r1.BackColor = Color.FloralWhite;
			g7r2.BackColor = Color.FloralWhite;
			g7r3.BackColor = Color.FloralWhite;
			g7r4.BackColor = Color.FloralWhite;
			
			g8r1.BackColor = Color.FloralWhite;
			g8r2.BackColor = Color.FloralWhite;
			g8r3.BackColor = Color.FloralWhite;
			g8r4.BackColor = Color.FloralWhite;
			
			g9r1.BackColor = Color.FloralWhite;
			g9r2.BackColor = Color.FloralWhite;
			g9r3.BackColor = Color.FloralWhite;
			g9r4.BackColor = Color.FloralWhite;
			
			g10r1.BackColor = Color.FloralWhite;
			g10r2.BackColor = Color.FloralWhite;
			g10r3.BackColor = Color.FloralWhite;
			g10r4.BackColor = Color.FloralWhite;
			
			//Reset solution blocks
			solution1Label.BackColor = Color.FloralWhite;
			solution2Label.BackColor = Color.FloralWhite;
			solution3Label.BackColor = Color.FloralWhite;
			solution4Label.BackColor = Color.FloralWhite;
			
			//Reset variables
			turn = 0;
			g1 = 0;
			g2 = 0;
			g3 = 0;
			g4 = 0;
			s1 = -1;
			s2 = -1;
			s3 = -1;
			s4 = -1;
			solution = false;
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			//Reset solution
			GenerateSolution();
			
			//Display game ready message
			MessageBox.Show("The game is ready to go! Click 'ok' to continue!");
		}
		
		//Methods that correspond to each turn. These fuckers are all the same.
		void Guess1()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g1c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g1c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g1c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g1c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g1c1Label.BackColor = Color.Red;
					break;
				case 5:
					g1c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g1c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g1c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g1c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g1c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g1c2Label.BackColor = Color.Red;
					break;
				case 5:
					g1c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g1c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g1c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g1c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g1c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g1c3Label.BackColor = Color.Red;
					break;
				case 5:
					g1c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g1c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g1c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g1c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g1c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g1c4Label.BackColor = Color.Red;
					break;
				case 5:
					g1c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess2()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g2c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g2c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g2c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g2c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g2c1Label.BackColor = Color.Red;
					break;
				case 5:
					g2c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g2c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g2c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g2c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g2c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g2c2Label.BackColor = Color.Red;
					break;
				case 5:
					g2c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g2c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g2c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g2c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g2c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g2c3Label.BackColor = Color.Red;
					break;
				case 5:
					g2c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g2c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g2c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g2c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g2c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g2c4Label.BackColor = Color.Red;
					break;
				case 5:
					g2c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess3()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g3c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g3c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g3c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g3c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g3c1Label.BackColor = Color.Red;
					break;
				case 5:
					g3c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g3c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g3c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g3c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g3c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g3c2Label.BackColor = Color.Red;
					break;
				case 5:
					g3c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g3c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g3c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g3c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g3c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g3c3Label.BackColor = Color.Red;
					break;
				case 5:
					g3c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g3c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g3c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g3c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g3c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g3c4Label.BackColor = Color.Red;
					break;
				case 5:
					g3c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess4()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g4c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g4c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g4c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g4c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g4c1Label.BackColor = Color.Red;
					break;
				case 5:
					g4c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g4c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g4c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g4c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g4c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g4c2Label.BackColor = Color.Red;
					break;
				case 5:
					g4c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g4c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g4c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g4c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g4c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g4c3Label.BackColor = Color.Red;
					break;
				case 5:
					g4c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g4c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g4c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g4c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g4c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g4c4Label.BackColor = Color.Red;
					break;
				case 5:
					g4c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess5()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g5c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g5c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g5c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g5c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g5c1Label.BackColor = Color.Red;
					break;
				case 5:
					g5c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g5c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g5c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g5c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g5c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g5c2Label.BackColor = Color.Red;
					break;
				case 5:
					g5c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g5c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g5c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g5c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g5c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g5c3Label.BackColor = Color.Red;
					break;
				case 5:
					g5c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g5c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g5c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g5c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g5c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g5c4Label.BackColor = Color.Red;
					break;
				case 5:
					g5c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess6()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g6c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g6c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g6c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g6c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g6c1Label.BackColor = Color.Red;
					break;
				case 5:
					g6c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g6c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g6c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g6c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g6c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g6c2Label.BackColor = Color.Red;
					break;
				case 5:
					g6c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g6c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g6c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g6c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g6c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g6c3Label.BackColor = Color.Red;
					break;
				case 5:
					g6c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g6c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g6c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g6c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g6c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g6c4Label.BackColor = Color.Red;
					break;
				case 5:
					g6c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess7()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g7c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g7c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g7c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g7c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g7c1Label.BackColor = Color.Red;
					break;
				case 5:
					g7c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g7c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g7c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g7c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g7c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g7c2Label.BackColor = Color.Red;
					break;
				case 5:
					g7c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g7c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g7c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g7c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g7c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g7c3Label.BackColor = Color.Red;
					break;
				case 5:
					g7c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g7c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g7c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g7c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g7c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g7c4Label.BackColor = Color.Red;
					break;
				case 5:
					g7c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess8()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g8c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g8c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g8c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g8c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g8c1Label.BackColor = Color.Red;
					break;
				case 5:
					g8c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g8c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g8c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g8c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g8c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g8c2Label.BackColor = Color.Red;
					break;
				case 5:
					g8c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g8c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g8c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g8c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g8c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g8c3Label.BackColor = Color.Red;
					break;
				case 5:
					g8c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g8c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g8c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g8c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g8c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g8c4Label.BackColor = Color.Red;
					break;
				case 5:
					g8c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess9()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g9c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g9c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g9c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g9c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g9c1Label.BackColor = Color.Red;
					break;
				case 5:
					g9c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g9c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g9c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g9c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g9c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g9c2Label.BackColor = Color.Red;
					break;
				case 5:
					g9c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g9c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g9c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g9c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g9c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g9c3Label.BackColor = Color.Red;
					break;
				case 5:
					g9c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g9c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g9c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g9c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g9c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g9c4Label.BackColor = Color.Red;
					break;
				case 5:
					g9c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		void Guess10()
		{
			GetGuessColors();
			
			switch(g1)
			{
				case 0:
					g10c1Label.BackColor = Color.Gold;
					break;
				case 1:
					g10c1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g10c1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g10c1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g10c1Label.BackColor = Color.Red;
					break;
				case 5:
					g10c1Label.BackColor = Color.Black;
					break;
			}
			
			switch(g2)
			{
				case 0:
					g10c2Label.BackColor = Color.Gold;
					break;
				case 1:
					g10c2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g10c2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g10c2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g10c2Label.BackColor = Color.Red;
					break;
				case 5:
					g10c2Label.BackColor = Color.Black;
					break;
			}
			
			switch(g3)
			{
				case 0:
					g10c3Label.BackColor = Color.Gold;
					break;
				case 1:
					g10c3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g10c3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g10c3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g10c3Label.BackColor = Color.Red;
					break;
				case 5:
					g10c3Label.BackColor = Color.Black;
					break;
			}
			
			switch(g4)
			{
				case 0:
					g10c4Label.BackColor = Color.Gold;
					break;
				case 1:
					g10c4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					g10c4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					g10c4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					g10c4Label.BackColor = Color.Red;
					break;
				case 5:
					g10c4Label.BackColor = Color.Black;
					break;
			}
		}
		
		//Get user inputed colors
		void GetGuessColors()
		{
			g1 = g1ComboBox.SelectedIndex;
			g2 = g2ComboBox.SelectedIndex;
			g3 = g3ComboBox.SelectedIndex;
			g4 = g4ComboBox.SelectedIndex;
		}
		
		//Determine if user inputed the right colors
		void CheckSolution()
		{
			if ((g1 == s1) && (g2 == s2) && (g3 == s3) && (g4 == s4))
			{
				solution = true;
			}
			else
			{
				solution = false;
			}
		}
		
		//Shows the solution
		void ShowSolution()
		{
			switch(s1)
			{
				case 0:
					solution1Label.BackColor = Color.Gold;
					break;
				case 1:
					solution1Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					solution1Label.BackColor = Color.DeepPink;
					break;
				case 3:
					solution1Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					solution1Label.BackColor = Color.Red;
					break;
				case 5:
					solution1Label.BackColor = Color.Black;
					break;
			}
			
			switch(s2)
			{
				case 0:
					solution2Label.BackColor = Color.Gold;
					break;
				case 1:
					solution2Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					solution2Label.BackColor = Color.DeepPink;
					break;
				case 3:
					solution2Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					solution2Label.BackColor = Color.Red;
					break;
				case 5:
					solution2Label.BackColor = Color.Black;
					break;
			}
			
			switch(s3)
			{
				case 0:
					solution3Label.BackColor = Color.Gold;
					break;
				case 1:
					solution3Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					solution3Label.BackColor = Color.DeepPink;
					break;
				case 3:
					solution3Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					solution3Label.BackColor = Color.Red;
					break;
				case 5:
					solution3Label.BackColor = Color.Black;
					break;
			}
			
			switch(s4)
			{
				case 0:
					solution4Label.BackColor = Color.Gold;
					break;
				case 1:
					solution4Label.BackColor = Color.DodgerBlue;
					break;
				case 2:
					solution4Label.BackColor = Color.DeepPink;
					break;
				case 3:
					solution4Label.BackColor = Color.LawnGreen;
					break;
				case 4:
					solution4Label.BackColor = Color.Red;
					break;
				case 5:
					solution4Label.BackColor = Color.Black;
					break;
			}
		}
		
		//Generates the correct solution
		void GenerateSolution()
		{
			s1 = AnsGen.Next(0, 6); //Based on index numbers.
			s2 = AnsGen.Next(0, 6);
			s3 = AnsGen.Next(0, 6);
			s4 = AnsGen.Next(0, 6);
			
			//Detect any ungenerated solution variables and display error messages before closing.
			if(s1 == -1 || s2 == -1 || s3 == -1 || s4 == -1)
			{
				if(s1 == -1)
				{
					MessageBox.Show("ERROR! s1 not generated.");
				}
				
				if(s2 == -1)
				{
					MessageBox.Show("ERROR! s2 not generated.");
				}
				
				if(s3 == -1)
				{
					MessageBox.Show("ERROR! s3 not generated.");
				}
				
				if(s4 == -1)
				{
					MessageBox.Show("ERROR! s4 not generated.");
				}
				
				this.Close();
			}
			
			//MessageBox.Show(s1 + ", " + s2 + ", " + s3 + ", " + s4); //Show the solution for testing
		}
		
		//Displays the results as purple and green pegs
		void DisplayResults()
		{
			switch(turn)
			{
				case 0:
					MessageBox.Show("ERROR 0"); //displays message if no turn is counted
					break;
				case 1:
					Results1Purple();
					Results1Green();
					break;
				case 2:
					Results2Purple();
					Results2Green();
					break;
				case 3:
					Results3Purple();
					Results3Green();
					break;
				case 4:
					Results4Purple();
					Results4Green();
					break;
				case 5:
					Results5Purple();
					Results5Green();
					break;
				case 6:
					Results6Purple();
					Results6Green();
					break;
				case 7:
					Results7Purple();
					Results7Green();
					break;
				case 8:
					Results8Purple();
					Results8Green();
					break;
				case 9:
					Results9Purple();
					Results9Green();
					break;
				case 10:
					Results10Purple();
					Results10Green();
					break;
				case 11:
					MessageBox.Show("ERROR 11"); //displays message if 11th turn detected
					break;
			}
		}
		
		/* I think that a good way of calculating results is displaying the white pegs and then
 		* cascade displaying the red pegs on top. This will require 20 methods. I attempted
 		* to do it in one method for both colors, but it got messy and created new issues. 
 		* NOTE: Purple will replace white pegs and green will replace red pegs for visibility.*/
 		
 		/* BUG: Not exactly sure why, but purple pegs will occasionally appear for duplicated solutions.
 		 * For example, if s1 and s2 are identical in value, and you input that value using the indexes
 		 * of the dropdown menu for all g-variables. Purple pegs may appear for g3 despite both 
 		 * s1Used and s2Used should be flipped already. I have yet to see a purple peg appear where
 		 * a green peg should have though.*/
 		
 		//Display correct color, correct place
		void Results1Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g1r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g1r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g1r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g1r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results2Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g2r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g2r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g2r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g2r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results3Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g3r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g3r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g3r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g3r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results4Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g4r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g4r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g4r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g4r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results5Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g5r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g5r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g5r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g5r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results6Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g6r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g6r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g6r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g6r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results7Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g7r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g7r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g7r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g7r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results8Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g8r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g8r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g8r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g8r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results9Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g9r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g9r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g9r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g9r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
		
		void Results10Green()
		{
			s1Used = false;
			s2Used = false;
			s3Used = false;
			s4Used = false;
			
			if(g1 == s1)
			{
				g10r1.BackColor = Color.Green;
				s1Used = true;
			}
			
			if(g2 == s2)
			{
				g10r2.BackColor = Color.Green;
				s2Used = true;
			}
			
			if(g3 == s3)
			{
				g10r3.BackColor = Color.Green;
				s3Used = true;
			}
			
			if(g4 == s4)
			{
				g10r4.BackColor = Color.Green;
				s4Used = true;
			}
		}
 		
 		//Display the correct color, wrong place
		void Results1Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g1r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g1r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g1r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g1r1.BackColor = Color.Purple;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g1r2.BackColor = Color.Purple;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g1r3.BackColor = Color.Purple;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results2Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g2r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g2r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g2r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g2r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g2r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g2r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g2r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g2r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g2r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g2r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g2r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g2r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g2r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results3Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g3r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g3r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g3r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g3r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g3r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g3r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g3r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g3r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g3r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g3r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g3r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g3r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g3r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results4Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g4r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g4r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g4r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g4r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g4r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g4r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g4r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g4r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g4r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g4r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g4r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g4r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g4r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results5Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g5r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g5r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g5r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g5r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g5r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g5r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g5r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g5r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g5r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g5r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g5r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g5r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g5r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results6Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g6r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g6r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g6r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g6r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g6r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g6r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g6r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g6r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g6r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g6r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g6r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g6r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g6r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results7Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g7r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g7r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g7r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g7r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g7r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g7r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g7r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g7r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g7r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g7r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g7r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g7r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g7r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results8Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g8r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g8r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g8r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g8r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g8r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g8r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g8r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g8r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g8r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g8r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g8r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g8r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g8r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results9Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g9r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g9r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g9r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g9r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g9r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g9r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g9r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g9r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g9r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g9r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g9r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g9r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g9r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
		
		void Results10Purple() 
		{
			if(g1 == s1 && s1Used == false)
			{
				g10r1.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g1 == s2 && s2Used == false)
			{
				g10r1.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g1 == s3 && s3Used == false)
			{
				g10r1.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g1 == s4 && s4Used == false)
			{
				g10r1.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g2 == s1 && s1Used == false)
			{
				g10r2.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g2 == s2 && s2Used == false)
			{
				g1r2.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g2 == s3 && s3Used == false)
			{
				g10r2.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g2 == s4 && s4Used == false)
			{
				g10r2.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g3 == s1 && s1Used == false)
			{
				g10r3.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g3 == s2 && s2Used == false)
			{
				g10r3.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g3 == s3 && s3Used == false)
			{
				g1r3.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g3 == s4 && s4Used == false)
			{
				g10r3.BackColor = Color.Purple;
				s4Used = true;
			}
			
			if(g4 == s1 && s1Used == false)
			{
				g10r4.BackColor = Color.Purple;
				s1Used = true;
			}
			else if(g4 == s2 && s2Used == false)
			{
				g10r4.BackColor = Color.Purple;
				s2Used = true;
			}
			else if(g4 == s3 && s3Used == false)
			{
				g10r4.BackColor = Color.Purple;
				s3Used = true;
			}
			else if(g4 == s4 && s4Used == false)
			{
				g1r4.BackColor = Color.Purple;
				s4Used = true;
			}
		}
	}
}